//
//  PaymentTests.swift
//  MELIAppTests
//
//  Created by Nicolás Cantó on 14-03-22.
//

import XCTest

@testable import MELIApp

class PaymentTests: XCTestCase {
    
    weak var presenter: PaymentPresenterProtocol?
    var viewController: PaymentViewController?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.viewController = PaymentFactory.getModule()
        self.viewController?.presenter?.interactor?.presenter = self
        self.presenter = self.viewController?.presenter
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCards() throws {
        test("Testing getCards") {
            PaymentStubs().getCards_200()
            self.presenter?.getCards()
        }
    }
    
    func testBanks() throws {
        test("Testing getBanks") {
            _ = XCTWaiter.wait(for: [expectation(description: "Wait for next test")], timeout: 2.0)
            self.presenter?.interactor?.dataSource?.selectionFlow.card = CardEntity(id: "visa", payment_type_id: "credit_card", name: "Visa", secure_thumbnail: nil)
            PaymentStubs().getBanks_200()
            self.presenter?.getBanks()
        }
    }
    
    func testInstallments() throws {
        test("TEST installments count") {
            _ = XCTWaiter.wait(for: [expectation(description: "Wait for next test")], timeout: 3.0)
            self.presenter?.interactor?.dataSource?.selectionFlow.card = CardEntity(id: "visa", payment_type_id: "credit_card", name: "Visa", secure_thumbnail: nil)
            self.presenter?.interactor?.dataSource?.selectionFlow.bank = BankEntity(id: "303", name: "Test bank", secure_thumbnail: nil)
            self.presenter?.interactor?.dataSource?.selectionFlow.amount = 3030
            PaymentStubs().getInstallments_200()
            self.presenter?.getInstallments()
        }
    }
    
    func testSelectionFlow() throws {
        test("Testing SelectionFlow") {
            _ = XCTWaiter.wait(for: [expectation(description: "Wait for next test")], timeout: 4.0)
            self.presenter?.setSelectionFlow(selectionFlow: SelectionFlow(
                                                amount: 3030,
                                                card: CardEntity(id: "visa", payment_type_id: "credit_card", name: "Visa", secure_thumbnail: nil),
                                                bank: BankEntity(id: "303", name: "Test bank", secure_thumbnail: nil),
                                                payerCosts: PayerCostsEntity(recommended_message: "Hello World! give me $5 dollars!")))
            XCTAssertNotNil(self.presenter?.getSelectionFlow().amount)
            XCTAssertNotNil(self.presenter?.getSelectionFlow().card)
            XCTAssertNotNil(self.presenter?.getSelectionFlow().bank)
            XCTAssertNotNil(self.presenter?.getSelectionFlow().payerCosts)
        }
    }

}

extension PaymentTests: PaymentInteractorToPresenterProtocol {
    func paymentMethodsCardsSuccess(cards: [CardEntity]) {
        XCTAssertTrue(cards.count == 11)
        for card in cards {
            XCTAssertTrue(card.payment_type_id == "credit_card")
            XCTAssertNotNil(card.id)
            XCTAssertNotNil(card.name)
            XCTAssertNotNil(card.secure_thumbnail)
        }
    }

    func paymentMethodsBanksSuccess(banks: [BankEntity]) {
        XCTAssertTrue(banks.count == 17)
        for bank in banks {
            XCTAssertNotNil(bank.id)
            XCTAssertNotNil(bank.name)
            XCTAssertNotNil(bank.secure_thumbnail)
        }
    }

    func paymentMethodsInstallmentsSuccess(installments: [InstallmentEntity]) {
        XCTAssertTrue(installments.count == 1)
        for installment in installments {
            XCTAssertNotNil(installment.payer_costs)
            for payerCost in installment.payer_costs ?? [] {
                XCTAssertNotNil(payerCost.recommended_message)
            }
        }
    }

    func paymentMethodsCardsFailure(error: String) {

    }

    func paymentMethodsBanksFailure(error: String) {

    }

    func paymentMethodsInstallmentsFailure(error: String) {

    }
}
extension XCTestCase {
    func test<T>(_ description: String, block: () throws -> T) rethrows -> T {
        try XCTContext.runActivity(named: description, block: { _ in try block() })
    }
}
