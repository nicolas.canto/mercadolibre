//
//  PaymentStubs.swift
//  MELIAppTests
//
//  Created by Nicolás Cantó on 14-03-22.
//

import OHHTTPStubs

class PaymentStubs {
    func getCards_200() {
        let getdFile = "getCards_200.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 200,
                headers: [:]
            )
        }
    }
    
    func getBanks_200() {
        let getdFile = "getBanks_200.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 200,
                headers: [:]
            )
        }
    }
    
    func getInstallments_200() {
        let getdFile = "getInstallments_200.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 200,
                headers: [:]
            )
        }
    }
}
