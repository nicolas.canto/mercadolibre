//
//  PaymentPresenter.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 12-03-22.
//

final class PaymentPresenter {
    weak var view: PaymentViewControllerProtocol?
    var interactor: PaymentInteractorProtocol?
    var router: PaymentRouterProtocol?
    weak var cardView: CardsViewProtocol?
    weak var banksView: BanksViewProtocol?
    weak var installmentsView: InstallmentsViewProtocol?
    
    init () {
        print("alloc \(self)")
    }
    deinit {
        print("dealloc \(self)")
    }
}

extension PaymentPresenter: PaymentPresenterProtocol {
    func getSelectionFlow() -> SelectionFlow {
        guard let seletionFlow = self.interactor?.dataSource?.selectionFlow else {
            return SelectionFlow(amount: nil, card: nil, bank: nil, payerCosts: nil)
        }
        return seletionFlow
    }
    
    func setSelectionFlow(selectionFlow: SelectionFlow) {
        self.interactor?.dataSource?.selectionFlow = selectionFlow
    }
    
    func setCardsView(view: CardsViewController) {
        self.cardView = view
    }
    
    func setBanksView(view: BanksViewController) {
        self.banksView = view
    }
    
    func setInstallmentsView(view: InstallmentsViewController) {
        self.installmentsView = view
    }
    
    func getCards() {
        self.interactor?.getCardsFromService()
    }
    
    func getBanks() {
        self.interactor?.getBanksFromService()
    }
    
    func getInstallments() {
        self.interactor?.getInstallmentsFromService()
    }
    
    func goToCards() {
        self.router?.showCards()
    }
    
    func goToBanks() {
        self.router?.showBanks()
    }
    
    func goToInstallments() {
        self.router?.showInstallments()
    }
    
    func goToPayment() {
        self.router?.finishFlow()
    }
}

extension PaymentPresenter: PaymentInteractorToPresenterProtocol {
    func paymentMethodsCardsSuccess(cards: [CardEntity]) {
        self.cardView?.loadCards(cards: cards)
    }
    
    func paymentMethodsBanksSuccess(banks: [BankEntity]) {
        self.banksView?.loadBanks(banks: banks)
    }
    
    func paymentMethodsInstallmentsSuccess(installments: [InstallmentEntity]) {
        self.installmentsView?.loadInstallments(installments: installments)
    }
    
    func paymentMethodsCardsFailure(error: String) {
        self.view?.onFailureCards(error: error)
    }
    
    func paymentMethodsBanksFailure(error: String) {
        self.view?.onFailureBanks(error: error)
    }
    
    func paymentMethodsInstallmentsFailure(error: String) {
        self.view?.onFailureIntallments(error: error)
    }
}
