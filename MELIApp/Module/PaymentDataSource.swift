//
//  PaymentDataSource.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 14-03-22.
//

import UIKit

final class PaymentDataSource: PaymentDataSourceProtocol {
    var cards: [CardEntity]?
    var banks: [BankEntity]?
    var installments: [InstallmentEntity]?
    var selectionFlow: SelectionFlow = SelectionFlow(amount: nil, card: nil, bank: nil, payerCosts: nil)
    
    func setCards(cards: [CardEntity]) {
        self.cards = cards
    }
    
    func setBanks(banks: [BankEntity]) {
        self.banks = banks
    }
    
    func setInstallments(installments: [InstallmentEntity]) {
        self.installments = installments
    }
    
    func getCards() -> [CardEntity]? {
        return self.cards
    }
    
    func getCreditCards() -> [CardEntity]? {
        return self.cards?.filter({ $0.payment_type_id == "credit_card" })
    }
    
    func getBanks() -> [BankEntity]? {
        return self.banks
    }
    
    func getInstallments() -> [InstallmentEntity]? {
        return self.installments
    }
}
