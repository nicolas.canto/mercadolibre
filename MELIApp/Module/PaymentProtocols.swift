//
//  PaymentProtocols.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 12-03-22.
//

// MARK: Module Protocols

import Foundation
import UIKit

protocol PaymentRouterProtocol: AnyObject {
    var presenter: PaymentPresenterProtocol? { get set }
    var viewController: PaymentViewController? { get set }
    func showCards()
    func showBanks()
    func showInstallments()
    func finishFlow()
}

protocol PaymentViewControllerProtocol: AnyObject {
    var presenter: PaymentPresenterProtocol? { get set }
    func showAlert(title: String?, message: String?)
    func showLoading(view: UIView)
    func hideLoading()
    func getAmount() -> Double
    func nextStep()
    func onFailureCards(error: String)
    func onFailureBanks(error: String)
    func onFailureIntallments(error: String)
}

protocol PaymentPresenterProtocol: AnyObject {
    var view: PaymentViewControllerProtocol? { get set }
    var interactor: PaymentInteractorProtocol? { get set }
    var router: PaymentRouterProtocol? { get set }
    var cardView: CardsViewProtocol? { get set }
    var banksView: BanksViewProtocol? { get set }
    var installmentsView: InstallmentsViewProtocol? { get set }
    func setCardsView(view: CardsViewController)
    func setBanksView(view: BanksViewController)
    func setInstallmentsView(view: InstallmentsViewController)
    func getCards()
    func getBanks()
    func getInstallments()
    func goToCards()
    func goToBanks()
    func goToInstallments()
    func goToPayment()
    func setSelectionFlow(selectionFlow: SelectionFlow)
    func getSelectionFlow() -> SelectionFlow
}

protocol PaymentInteractorProtocol: AnyObject {
    var presenter: PaymentInteractorToPresenterProtocol? { get set }
    var service: PaymentServiceProtocol? { get set }
    var dataSource: PaymentDataSourceProtocol? { get set }
    func getCardsFromService()
    func getBanksFromService()
    func getInstallmentsFromService()
}

protocol PaymentInteractorToPresenterProtocol: AnyObject {
    func paymentMethodsCardsSuccess(cards: [CardEntity])
    func paymentMethodsBanksSuccess(banks: [BankEntity])
    func paymentMethodsInstallmentsSuccess(installments: [InstallmentEntity])
    func paymentMethodsCardsFailure(error: String)
    func paymentMethodsBanksFailure(error: String)
    func paymentMethodsInstallmentsFailure(error: String)
}

protocol PaymentServiceProtocol: AnyObject {
    func getData(url: String, completion: @escaping (Result<Data?, Error>) -> Void)
}

protocol PaymentDataSourceProtocol: AnyObject {
    var cards: [CardEntity]? { get set }
    var banks: [BankEntity]? { get set }
    var installments: [InstallmentEntity]? { get set }
    var selectionFlow: SelectionFlow { get set }
    func setCards(cards: [CardEntity])
    func setBanks(banks: [BankEntity])
    func setInstallments(installments: [InstallmentEntity])
    func getCards() -> [CardEntity]?
    func getCreditCards() -> [CardEntity]?
    func getBanks() -> [BankEntity]?
    func getInstallments() -> [InstallmentEntity]?
}

// MARK: UI Protocols

protocol CardsViewProtocol: AnyObject {
    var banksView: BanksViewProtocol? { get set }
    var presenter: PaymentPresenterProtocol? { get set }
    func loadCards(cards: [CardEntity])
}

protocol BanksViewProtocol: AnyObject {
    var installmentsView: InstallmentsViewProtocol? { get set }
    var presenter: PaymentPresenterProtocol? { get set }
    func loadBanks(banks: [BankEntity])
}

protocol InstallmentsViewProtocol: AnyObject {
    var paymentView: PaymentViewControllerProtocol? { get set }
    var presenter: PaymentPresenterProtocol? { get set }
    func loadInstallments(installments: [InstallmentEntity])
}

