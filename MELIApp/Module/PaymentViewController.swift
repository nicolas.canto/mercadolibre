//
//  PaymentViewController.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 12-03-22.
//

import UIKit
import Foundation

final class PaymentViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var amount: UITextField!
    var presenter: PaymentPresenterProtocol?
    var loadingViewController: LoadingViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Pago"
        amount.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        amount.delegate = self
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @IBAction func onTapNext(_ sender: Any) {
        if self.getAmount() == 0 {
            self.showAlert(title: "Inválido", message: "Ingresa un monto a pagar.")
            return
        }
        self.nextStep()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        self.presenter?.interactor?.dataSource?.selectionFlow.amount = Utils.stringToDouble(text: text)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil) else {
            return false
        }
        return true
    }
}

extension PaymentViewController: PaymentViewControllerProtocol {
    func showAlert(title: String? = nil, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getAmount() -> Double {
        guard let amount = self.presenter?.interactor?.dataSource?.selectionFlow.amount else {
            return 0
        }
        return amount
    }
    
    func nextStep() {
        self.presenter?.goToCards()
    }
    
    func showLoading(view: UIView) {
        self.loadingViewController = LoadingViewController()
        view.addSubview(self.loadingViewController?.view ?? LoadingViewController().view)
    }
    
    func hideLoading() {
        self.loadingViewController?.view.removeFromSuperview()
    }
    
    func onFailureCards(error: String) {
        self.hideLoading()
        self.showAlert(title: "Error", message: error)
    }
    
    func onFailureBanks(error: String) {
        self.hideLoading()
        self.showAlert(title: "Error", message: error)
    }
    
    func onFailureIntallments(error: String) {
        self.hideLoading()
        self.showAlert(title: "Error", message: error)
    }
}
