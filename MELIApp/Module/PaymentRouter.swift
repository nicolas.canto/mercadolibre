//
//  PaymentRouter.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 12-03-22.
//

import Foundation
import UIKit

class PaymentRouter {
    weak var presenter: PaymentPresenterProtocol?
    var viewController: PaymentViewController?
    
    init () {
        print("alloc \(self)")
    }
    deinit {
        print("dealloc \(self)")
    }
}

extension PaymentRouter: PaymentRouterProtocol {
    func showCards() {
        let cardsViewController = CardsViewController()
        self.presenter?.setCardsView(view: cardsViewController)
        cardsViewController.presenter = self.presenter
        self.viewController?.navigationController?.pushViewController(cardsViewController, animated: true)
    }
    
    func showBanks() {
        let banksViewController = BanksViewController()
        self.presenter?.setBanksView(view: banksViewController)
        banksViewController.presenter = self.presenter
        self.viewController?.navigationController?.pushViewController(banksViewController, animated: true)
    }
    
    func showInstallments() {
        let installmentsViewController = InstallmentsViewController()
        self.presenter?.setInstallmentsView(view: installmentsViewController)
        installmentsViewController.presenter = self.presenter
        self.viewController?.navigationController?.pushViewController(installmentsViewController, animated: true)
    }
    
    func finishFlow() {
        guard let selectionFlow = self.presenter?.getSelectionFlow() else {
            return
        }
        print("finish with: \(selectionFlow)")
        self.viewController?.navigationController?.popToViewController(self.viewController ?? UIViewController(), animated: true)
        let amount = selectionFlow.amount ?? 0.0
        let card = selectionFlow.card?.name ?? ""
        let bank = selectionFlow.bank?.name ?? ""
        let payerCosts = selectionFlow.payerCosts?.recommended_message ?? ""
        let message = "Monto a pagar: $\(amount.setFormat(2))\nMétodo de pago: \(card)\nBanco: \(bank)\nCantidada de cuotas: \(payerCosts)"
        self.presenter?.view?.showAlert(title: "Resumen", message: message)
    }
}
