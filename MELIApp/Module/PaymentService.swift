//
//  PaymentService.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 12-03-22.
//

import Alamofire

class PaymentService {
    
    init () {
        print("alloc \(self)")
    }
    deinit {
        print("dealloc \(self)")
    }
}

extension PaymentService: PaymentServiceProtocol {
    func getData(url: String, completion: @escaping (Result<Data?, Error>) -> Void) {
        AF.request(url).response { response in
            switch response.result {
            case .success(let data):
                debugPrint("sucess: \(String(describing: data))")
                completion(.success(data))
            case .failure(let error):
                debugPrint("failure: \(error)")
                completion(.failure(error))
            }
        }
    }
}
