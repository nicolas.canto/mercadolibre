//
//  PaymentEntity.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 12-03-22.
//

struct CardEntity: Codable {
    var id: String?
    var payment_type_id: String?
    var name: String?
    var secure_thumbnail: String?
}

struct BankEntity: Codable {
    var id: String?
    var name: String?
    var secure_thumbnail: String?
}

struct InstallmentEntity: Codable {
    var payer_costs: [PayerCostsEntity]?
}

struct PayerCostsEntity: Codable {
    var recommended_message: String?
}

struct SelectionFlow {
    var amount: Double?
    var card: CardEntity?
    var bank: BankEntity?
    var payerCosts: PayerCostsEntity?
}
