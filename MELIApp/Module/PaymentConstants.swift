//
//  PaymentConstants.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 13-03-22.
//

enum PaymentConstants {
    enum Endpoints {
        static let cards =  "https://api.mercadopago.com/v1/payment_methods?public_key=444a9ef5-8a6b-429f-abdf-587639155d88#json"
        static let banks = "https://api.mercadopago.com/v1/payment_methods/card_issuers?public_key=444a9ef5-8a6b-429f-abdf-587639155d88&payment_method_id="
        static let installments = "https://api.mercadopago.com/v1/payment_methods/installments?public_key=444a9ef5-8a6b-429f-abdf-587639155d88&amount="
    }
    enum Cell {
        static let standardViewCellNib = "StandardTableViewCell"
        static let standardReuseIdentifier = "standardReuseIdentifier"
        static let installmentViewCellNib = "InstallmentsTableViewCell"
        static let installmentReuseIdentifier = "installmentReuseIdentifier"
    }
}
