//
//  PaymentFactory.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 12-03-22.
//

final class PaymentFactory {
    
    static func getModule() -> PaymentViewController {
        let view = PaymentViewController()
        let presenter: PaymentPresenterProtocol & PaymentInteractorToPresenterProtocol = PaymentPresenter()
        let interactor = PaymentInteractor()
        let service = PaymentService()
        let router = PaymentRouter()
        let dataSource = PaymentDataSource()
        
        interactor.dataSource = dataSource
        interactor.service = service
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        router.viewController = view
        router.presenter = presenter
        interactor.presenter = presenter
        
        return view
    }
}
