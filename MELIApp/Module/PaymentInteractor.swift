//
//  PaymentInteractor.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 12-03-22.
//

import Foundation

final class PaymentInteractor {
    weak var presenter: PaymentInteractorToPresenterProtocol?
    var service: PaymentServiceProtocol?
    var dataSource: PaymentDataSourceProtocol?
    
    init () {
        print("alloc \(self)")
    }
    deinit {
        print("dealloc \(self)")
    }
    
    private func getCreditCards(cards: [CardEntity]) -> [CardEntity]? {
        return cards.filter({ $0.payment_type_id == "credit_card"})
    }
}

extension PaymentInteractor: PaymentInteractorProtocol {
    func getCardsFromService() {
        self.service?.getData(url: PaymentConstants.Endpoints.cards, completion: { [weak self] result in
            switch result {
            case .success(let data):
                guard let data = data else {
                    self?.presenter?.paymentMethodsCardsFailure(error: "Error al obtener cards de servicio.")
                    return
                }
                
                do {
                    let cards = try JSONDecoder().decode([CardEntity].self, from: data)
                    guard let creditCards = self?.getCreditCards(cards: cards) else {
                        self?.presenter?.paymentMethodsCardsFailure(error: "No existen tarjetas de crédito")
                        return
                    }
                    self?.dataSource?.setCards(cards: creditCards)
                    self?.presenter?.paymentMethodsCardsSuccess(cards: creditCards)
                } catch {
                    self?.presenter?.paymentMethodsCardsFailure(error: error.localizedDescription)
                }
                
            case .failure(let error):
                self?.presenter?.paymentMethodsCardsFailure(error: error.localizedDescription)
            }
        })
    }
    
    func getBanksFromService() {
        guard let cardSelected = self.dataSource?.selectionFlow.card?.id else {
            return
        }
        self.service?.getData(url: "\(PaymentConstants.Endpoints.banks)\(cardSelected)", completion: { [weak self] result in
            switch result {
            case .success(let data):
                guard let data = data else {
                    self?.presenter?.paymentMethodsBanksFailure(error: "Error al obtener banks de servicio.")
                    return
                }
                
                do {
                    let banks = try JSONDecoder().decode([BankEntity].self, from: data)
                    self?.dataSource?.setBanks(banks: banks)
                    self?.presenter?.paymentMethodsBanksSuccess(banks: banks)
                } catch {
                    self?.presenter?.paymentMethodsBanksFailure(error: error.localizedDescription)
                }
                
            case .failure(let error):
                self?.presenter?.paymentMethodsBanksFailure(error: error.localizedDescription)
            }
        })
    }
    
    func getInstallmentsFromService() {
        guard let cardSelected = self.dataSource?.selectionFlow.card?.id else {
            return
        }
        guard let bankSelected = self.dataSource?.selectionFlow.bank?.id else {
            return
        }
        guard let amount = self.dataSource?.selectionFlow.amount else {
            return
        }
        self.service?.getData(url: "\(PaymentConstants.Endpoints.installments)\(amount)&payment_method_id=\(cardSelected)&issuer.id=\(bankSelected)", completion: { [weak self] result in
            switch result {
            case .success(let data):
                guard let data = data else {
                    self?.presenter?.paymentMethodsInstallmentsFailure(error: "Error al obtener installments de servicio.")
                    return
                }
                
                do {
                    let installments = try JSONDecoder().decode([InstallmentEntity].self, from: data)
                    self?.dataSource?.setInstallments(installments: installments)
                    self?.presenter?.paymentMethodsInstallmentsSuccess(installments: installments)
                } catch {
                    self?.presenter?.paymentMethodsInstallmentsFailure(error: error.localizedDescription)
                }
                
            case .failure(let error):
                self?.presenter?.paymentMethodsInstallmentsFailure(error: error.localizedDescription)
            }
        })
    }
}
