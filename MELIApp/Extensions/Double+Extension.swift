//
//  Double+Extension.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 14-03-22.
//

import Foundation

extension Double {
    /// Return a thousands formatted number
    func setFormat(_ minFractionDigits: Int) -> String {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "es_CL")
        formatter.minimumFractionDigits = minFractionDigits
        return formatter.string(from: NSNumber(value: self)) ?? ""
    }
}
