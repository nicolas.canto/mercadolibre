//
//  Utils.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 13-03-22.
//

import Foundation

enum Utils {
    static func cleanAmount(text: String) -> String {
        var cleaned = String()
        if let range: Range<String.Index> = text.range(of: ",") {
            let index: Int = text.distance(from: text.startIndex, to: range.lowerBound)
            cleaned = "\(text.dropLast(text.count - index))".replacingOccurrences(of: ".", with: "")
            print("cleaned: \(cleaned)")
            return cleaned
        }
        print("text: \(text)")
        return text
    }
    
    static func stringToDouble(text: String?) -> Double {
        guard let decimal = (text as NSString?)?.doubleValue else {
            return 0.0
        }
        return decimal
    }
}
