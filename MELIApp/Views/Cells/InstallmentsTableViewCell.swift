//
//  InstallmentsTableViewCell.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 13-03-22.
//

import UIKit

class InstallmentsTableViewCell: UITableViewCell {

    @IBOutlet weak var installment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setInstallment(_ installment: String) {
        self.installment.text = installment
    }
    
}
