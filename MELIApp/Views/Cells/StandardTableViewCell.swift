//
//  StandardTableViewCell.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 13-03-22.
//

import UIKit

class StandardTableViewCell: UITableViewCell {

    @IBOutlet weak private var name: UILabel!
    @IBOutlet weak private var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setName(_ name: String) {
        self.name.text = name
    }
    
    func setIcon(_ icon: String) {
        guard let url = URL(string: icon) else {
            return
        }
        self.icon.load(url: url)
    }
}
