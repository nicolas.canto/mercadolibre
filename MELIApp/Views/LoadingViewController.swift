//
//  LoadingViewController.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 14-03-22.
//

import UIKit

class LoadingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.alpha = 0.5
    }
}
