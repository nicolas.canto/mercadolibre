//
//  CardsViewController.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 13-03-22.
//

import UIKit

final class CardsViewController: UIViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    var banksView: BanksViewProtocol?
    var presenter: PaymentPresenterProtocol?
    var cards: [CardEntity]?
    var cardSelected: CardEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Tarjetas"
        self.setTable()
        self.banksView = BanksViewController()
        self.presenter?.getCards()
        self.presenter?.view?.showLoading(view: self.view)
    }
    
    deinit {
        print("dealloc \(self)")
    }
    
    private func setTable() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let standardCellView = UINib(nibName: PaymentConstants.Cell.standardViewCellNib, bundle: nil)
        self.tableView.register(standardCellView, forCellReuseIdentifier: PaymentConstants.Cell.standardReuseIdentifier)
    }
}

extension CardsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cards?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let standardCell = self.tableView.dequeueReusableCell(withIdentifier: PaymentConstants.Cell.standardReuseIdentifier) as? StandardTableViewCell else {
            print("No se puede cargar StandardTableViewCell")
            return UITableViewCell()
        }
        standardCell.setName(self.cards?[indexPath.row].name ?? "")
        standardCell.setIcon(self.cards?[indexPath.row].secure_thumbnail ?? "")
        
        return standardCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.interactor?.dataSource?.selectionFlow.card = self.cards?[indexPath.row]
        self.presenter?.goToBanks()
    }
}

extension CardsViewController: CardsViewProtocol {
    func loadCards(cards: [CardEntity]) {
        self.presenter?.view?.hideLoading()
        self.cards = cards
        self.tableView.reloadData()
    }
}
