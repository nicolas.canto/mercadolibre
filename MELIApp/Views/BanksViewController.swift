//
//  BanksViewController.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 13-03-22.
//

import UIKit

class BanksViewController: UIViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    var installmentsView: InstallmentsViewProtocol?
    var presenter: PaymentPresenterProtocol?
    var banks: [BankEntity]?
    var bankSelected: BankEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Bancos"
        self.setTable()
        self.installmentsView = InstallmentsViewController()
        self.presenter?.getBanks()
        self.presenter?.view?.showLoading(view: self.view)
    }
    
    deinit {
        print("dealloc \(self)")
    }
    
    private func setTable() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let standardCellView = UINib(nibName: PaymentConstants.Cell.standardViewCellNib, bundle: nil)
        self.tableView.register(standardCellView, forCellReuseIdentifier: PaymentConstants.Cell.standardReuseIdentifier)
    }
}

extension BanksViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.banks?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let standardCell = self.tableView.dequeueReusableCell(withIdentifier: PaymentConstants.Cell.standardReuseIdentifier) as? StandardTableViewCell else {
            print("No se puede cargar StandardTableViewCell")
            return UITableViewCell()
        }
        standardCell.setName(self.banks?[indexPath.row].name ?? "")
        standardCell.setIcon(self.banks?[indexPath.row].secure_thumbnail ?? "")
        
        return standardCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.interactor?.dataSource?.selectionFlow.bank = self.banks?[indexPath.row]
        self.presenter?.goToInstallments()
    }
}

extension BanksViewController: BanksViewProtocol {
    func loadBanks(banks: [BankEntity]) {
        self.presenter?.view?.hideLoading()
        self.banks = banks
        self.tableView.reloadData()
    }
}
