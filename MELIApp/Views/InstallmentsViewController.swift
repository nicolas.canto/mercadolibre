//
//  InstallmentsViewController.swift
//  MELIApp
//
//  Created by Nicolás Cantó on 13-03-22.
//

import UIKit

class InstallmentsViewController: UIViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    var paymentView: PaymentViewControllerProtocol?
    var presenter: PaymentPresenterProtocol?
    var installments: [InstallmentEntity]?
    var installmentSelected: PayerCostsEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Cuotas"
        self.setTable()
        self.presenter?.getInstallments()
        self.presenter?.view?.showLoading(view: self.view)
    }
    
    deinit {
        print("dealloc \(self)")
    }
    
    private func setTable() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let installmentCellView = UINib(nibName: PaymentConstants.Cell.installmentViewCellNib, bundle: nil)
        self.tableView.register(installmentCellView, forCellReuseIdentifier: PaymentConstants.Cell.installmentReuseIdentifier)
    }

}

extension InstallmentsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.installments?.first?.payer_costs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let payerCosts = self.installments?.first?.payer_costs else {
            print("error al cargando installments.")
            return UITableViewCell()
        }
        
        guard let installmentCell = self.tableView.dequeueReusableCell(withIdentifier: PaymentConstants.Cell.installmentReuseIdentifier) as? InstallmentsTableViewCell else {
            print("No se puede cargar InstallmentsTableViewCell")
            return UITableViewCell()
        }
        installmentCell.setInstallment(payerCosts[indexPath.row].recommended_message ?? "")
        
        return installmentCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let payerCosts = self.installments?.first?.payer_costs else {
            print("error al cargando installments.")
            return
        }
        self.presenter?.interactor?.dataSource?.selectionFlow.payerCosts = payerCosts[indexPath.row]
        self.presenter?.goToPayment()
    }
}

extension InstallmentsViewController: InstallmentsViewProtocol {
    func loadInstallments(installments: [InstallmentEntity]) {
        self.presenter?.view?.hideLoading()
        self.installments = installments
        self.tableView.reloadData()
    }
}
